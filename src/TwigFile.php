<?php

final class TwigFile
{
    private $path;

    public function __construct($path)
    {
        $this->path = $path;
    }

    public function parse()
    {
        $content = file_get_contents($this->path);
        preg_match_all("#\{%\s*(extends|include)\s*[\"']([^\"']*)[\"']\s*%}#", $content, $matches);
        $relations = [];
        for ($i = 0; $i < count($matches[1]); $i++) {
            $relations[] = [
                $this->path,
                $matches[1][$i],
                $matches[2][$i]
            ];
        }
        return $relations;
    }
}