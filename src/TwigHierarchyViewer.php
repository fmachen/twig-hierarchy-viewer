<?php

final class TwigHierarchyViewer
{
    private $scanDir;
    private $trigrams = [];

    public function __construct($scanDir)
    {
        $this->scanDir = rtrim($scanDir, "/");
    }

    public function parse()
    {
        $this->parseDir($this->scanDir);
        $this->cleanTrigram();
        var_dump($this->trigrams);
    }

    private function parseDir($path)
    {
        if (!is_dir($path)) {
            return;
        }
        $handle = opendir($path);
        if (!$handle) {
            return;
        }
        while (($name = readdir($handle)) !== false) {
            if (substr($name, 0, 1) == ".") {
                continue;
            }
            if (is_dir($path . "/" . $name)) {
                $this->parseDir($path . "/" . $name);
            } elseif (substr($name, -5) == ".twig") {
                $this->parseFile($path . "/" . $name);
            }
        }
        closedir($handle);
    }

    private function parseFile($path)
    {
        $twig = new TwigFile($path);
        $this->mergeTrigram($twig->parse());
    }

    private function mergeTrigram($trigrams)
    {
        $this->trigrams = array_merge($this->trigrams, $trigrams);
    }

    private function cleanTrigram()
    {
        $this->trigrams = array_map(function ($item) {
            return [
                trim(substr($item[0], strlen($this->scanDir)), "/"),
                $item[1],
                $item[2]
            ];
        }, $this->trigrams);
    }
}