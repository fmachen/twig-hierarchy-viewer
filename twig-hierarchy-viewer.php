<?php

require "src/TwigHierarchyViewer.php";
require "src/TwigFile.php";

$scanDir = $argv[1] ?: ".";

$viewer = new TwigHierarchyViewer($scanDir);
$viewer->parse();
